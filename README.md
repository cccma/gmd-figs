# CanESM5 GMD Figures

This repository hosts the jupyter notebooks used to create the figures for the CanESM5 GMD paper. The paper is on google docs at:

    https://docs.google.com/document/d/1As7Vu8N2cuu1VpAHEs_cJtRilWdfp8m_5eJLove3_lM

NB: 

1. Beware of conflicts with other users when using the notebooks in the ppp2 directory (announce on slack #canesm-gmd). 
2. All files are .gitignored by default, only *.ipynb and *.py files are currently captured.

## Jupyter environment

A python3.7 miniconda environment is installed at:

    /home/ords/crd/ccrn/scrd104/miniconda3/bin/

and is used to run jupyter lab on ports:

- 40801 (Neil)
- 40802 (Andrew)
- 40803 (Sarah)

## List of figures and corresponding notebooks:
The notebooks include a figure number and a description incase figure numbers change.

- Model schematic?
- coupling?
- Diagram of experiments / other technical figures?
- optimization
- PI control summary timeseries:............................................................................fig-05_canesm5-pistab.ipynb 
- Summary skill diagram of one sort or another (e.g. Taylor diagram(s) or checkboard/Gleckler plot).........fig-06_canesm5_skill.ipynb
- a. TAS and b. PR global map - anomaly from obs over 1981-2010(?)..........................................fig-07_tas-pr-climanom.ipynb
- Zonal mean temperature/wind bias from obs over 1981-2010 / surface wind...................................fig-08_atmos_ua-ta-sec.ipynb
- Ocean zonal mean a. temperature b. salinity anomaly from obs (1980-2010) and c. MOC :.....................fig-09_ocean_t-s-moc.ipynb
- a. SST, b. SSS, c. ZOS bias relative to obs...............................................................fig-10_ocean_tos-sos.ipynb
- Sea-ice annual cycle in a. volume and b. extent (SH and NH)...............................................fig-11_seaice_vol-extent.ipynb
- Maps of sea-ice concentration climatology in a. March NH, b. March SH, c. Sep NH and d. Sep SH............fig-12_seaice_extent-maps.ipynb
- Meridional heat transports in the atmosphere? and ocean...................................................fig-13_ocean-mht.ipynb
- ENSO summary (spectra?), map?.............................................................................fig-14_enso.ipynb
- Ocean carbon cycle ocean e.g. DIC/NO3 zonal sections, zonal mean co2 flux in ocean........................fig-15_ocean_bgc-sec.ipynb / fig-15x_ocean_bgc-maps.ipynb
- Vivek to do - land biogeochemistry section................................................................ 
- A separate section on carbon cycle over the historical period  (Vivek to do this)......................... 
- GMST - global mean time-series: DECK+hist, for context....................................................
- a. GMST, blended and masked - global mean time-series: hist, and b. histogram of historical trends........ 
- Map and zonal mean of trends (CanESM2, CanESM5, obs)?.....................................................fig-20_tas_trends.ipynb 
- time-series of sea-ice extent + trends....................................................................fig-21_si_trends.ipynb
- cloud fraction ...........................................................................................fig-22_cloud_fraction.ipynb